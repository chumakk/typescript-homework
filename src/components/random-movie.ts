import { IMovie } from '../interfaces';
import * as ENV from '../enums';

const RandomMovie = (movie: IMovie): HTMLElement => {
    const background: string = movie.backdrop_path
        ? `url(${ENV.API.BACKDROP_IMAGE_URL}${movie.backdrop_path}) 0 0/cover no-repeat`
        : `url(${ENV.APP.IMAGE_BACKDROP_PLACEHOLDER}) 0 50%/cover no-repeat`;
        
    const elem: HTMLElement = document.createElement('section');
    elem.id = 'random-movie';
    elem.classList.add('py-5', 'text-center', 'container-fluid');
    elem.style.background = background;
    elem.innerHTML = `
    <div class="row py-lg-5">
        <div
            class="col-lg-6 col-md-8 mx-auto"
            style="background-color: #2525254f"
        >
            <h1 id="random-movie-name" class="fw-light text-light">
            ${movie.original_title}
            </h1>
            <p
                id="random-movie-description"
                class="lead text-white"
            >
            ${movie.overview}
            </p>
        </div>
    </div>`;

    return elem;
};

export default RandomMovie;
