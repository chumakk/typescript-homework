import { IMovie } from '../interfaces';
import * as ENV from '../enums';

const MovieCardTemplate = (
    movie: IMovie,
    likedMovies: IMovie['id'][]
): string => {
    const BASE_IMAGE_URL: string = ENV.API.BASE_IMAGE_URL;

    let imageUrl: string;
    if (movie.poster_path) {
        imageUrl = BASE_IMAGE_URL + movie.poster_path;
    } else {
        imageUrl = ENV.APP.IMAGE_PLACEHOLDER;
    }

    let release: string;
    if (movie.release_date !== undefined) {
        release =
            movie.release_date.length > 0
                ? movie.release_date
                : 'no release date';
    } else {
        release = 'not released';
    }

    const fill: string = likedMovies.includes(movie.id)
        ? ENV.APP.LIKEDHEARTCOL
        : ENV.APP.HEARTCOL;

    return `<div class="card shadow-sm">
    <img
        src='${imageUrl}'
    />
    <svg
        xmlns="http://www.w3.org/2000/svg"
        stroke="red"
        fill='${fill}'
        width="50"
        height="50"
        class="bi bi-heart-fill position-absolute p-2"
        viewBox="0 -2 18 22"
        data-film-id=${movie.id}
    >
        <path
            fill-rule="evenodd"
            d="M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z"
        />
    </svg>
    <div class="card-body">
        <p class="card-text truncate">${movie.overview}</p>
        <div
            class="
                d-flex
                justify-content-between
                align-items-center
            "
        >
            <small class="text-muted">${release}</small>
        </div>
    </div>
</div>`;
};

export default MovieCardTemplate;
