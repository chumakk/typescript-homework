import { IMovie } from '../interfaces';
import MovieCardTemplate from './movie-card-template';

const MovieFavCard = (
    movie: IMovie,
    likedMovies: IMovie['id'][]
): HTMLElement => {
    const html: string = MovieCardTemplate(movie, likedMovies);

    const elem: HTMLElement = document.createElement('div');
    elem.classList.add('col-12', 'p-2');
    elem.innerHTML = html;
    return elem;
};

export default MovieFavCard;
