enum API {
    API_KEY = '8606ac99ff408b52e929d31983cafc0d',
    BASE_MOVIE_URL = 'https://api.themoviedb.org/3',
    BASE_IMAGE_URL = 'https://image.tmdb.org/t/p/w500',
    BACKDROP_IMAGE_URL = 'https://image.tmdb.org/t/p/w780',
}

export { API };
