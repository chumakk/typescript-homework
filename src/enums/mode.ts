enum Mode {
    searchByName,
    popularMovies,
    topRatedMovies,
    upcomingMovies,
}

export { Mode };