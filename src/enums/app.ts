enum APP {
    IMAGE_PLACEHOLDER = 'http://lexingtonvenue.com/media/poster-placeholder.jpg',
    LIKEDHEARTCOL = 'red',
    HEARTCOL = '#ff000078',
    IMAGE_BACKDROP_PLACEHOLDER = 'https://i.pinimg.com/originals/ab/5f/db/ab5fdbc8da76c76427d8d96582e5371f.jpg',
}

export { APP };
