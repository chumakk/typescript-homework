import MovieApp from './movieApp';

export async function render(): Promise<void> {
    const app = new MovieApp();
    app.init();
}