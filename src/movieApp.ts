import { movie as Movie, storage as Storage } from './services';
import { IMovie } from './interfaces';
import MovieCard from './components/movie-card';
import MovieFavCard from './components/movie-fav-card';
import { APP, Mode } from './enums';
import RandomMovie from './components/random-movie';

class MovieApp {
    private mode: Mode;
    private page: number;
    private text: string;
    private likedMovies: IMovie['id'][] = [];
    private randomMovie: IMovie | null = null;
    constructor() {
        this.page = 1;
        this.mode = Mode.upcomingMovies;
        this.text = '';
        const likedMovies = Storage.getItem('likedMovies');
        if (likedMovies) {
            this.likedMovies = JSON.parse(Storage.getItem('likedMovies')!);
        }
    }

    init(): void {
        const searchSumbitButton: HTMLButtonElement | null =
            document.querySelector('#submit');
        const filmContainer: HTMLElement | null =
            document.querySelector('#film-container');
        const loadMoreButton = document.querySelector('#load-more');
        const searchEl: HTMLInputElement | null =
            document.querySelector('#search');
        const popularMovieButton: HTMLInputElement | null =
            document.querySelector('#popular');
        const topRatedMovieButton: HTMLInputElement | null =
            document.querySelector('#top_rated');

        const upcomingMovieButton: HTMLInputElement | null =
            document.querySelector('#upcoming');

        const favoriteMoviesButton: HTMLButtonElement | null =
            document.querySelector('.navbar-toggler');

        const favoriteMoviesContainer: HTMLElement | null =
            document.querySelector('#favorite-movies');

        popularMovieButton?.addEventListener('click', (): void => {
            this.setMode(filmContainer!, Mode.popularMovies);
            this.loadMovies(filmContainer!);
        });

        upcomingMovieButton?.addEventListener('click', (): void => {
            this.setMode(filmContainer!, Mode.upcomingMovies);
            this.loadMovies(filmContainer!);
        });

        topRatedMovieButton?.addEventListener('click', (): void => {
            this.setMode(filmContainer!, Mode.topRatedMovies);
            this.loadMovies(filmContainer!);
        });

        searchSumbitButton?.addEventListener('click', (): void => {
            const text = searchEl?.value;
            if (text?.length) {
                this.text = text;
                this.setMode(filmContainer!, Mode.searchByName);
                this.loadMovies(filmContainer!);
            }
        });

        loadMoreButton?.addEventListener('click', (): void => {
            this.page += 1;
            this.loadMovies(filmContainer!);
            history.pushState('loadmore', '', `/?page=${this.page}`);
        });

        filmContainer?.addEventListener('click', (e): void => {
            this.toggleFavoriteMovies(e);
        });

        favoriteMoviesContainer?.addEventListener('click', (e): void => {
            this.toggleFavoriteMovies(e);
        });

        favoriteMoviesButton?.addEventListener(
            'click',
            async (): Promise<void> => {
                const modal: HTMLElement | null = document.querySelector(
                    '.modal-backdrop.fade'
                );
                const favoriteCloseButton: HTMLButtonElement | null =
                    document.querySelector('.btn-close.text-reset');

                const onClose = (): void => {
                    this.cleanContainer(favoriteMoviesContainer!);
                    modal?.removeEventListener('click', onClose);
                    favoriteCloseButton?.removeEventListener('click', onClose);
                };

                modal?.addEventListener('click', onClose);
                favoriteCloseButton?.addEventListener('click', onClose);

                for (const id of [...this.likedMovies].reverse()) {
                    try {
                        const movie: IMovie = await Movie.getMovieById(id);
                        const elem: HTMLElement = MovieFavCard(
                            movie,
                            this.likedMovies
                        );
                        favoriteMoviesContainer?.append(elem);
                    } catch (error) {
                        console.log(error);
                    }
                }
            }
        );

        const checkedEl: HTMLInputElement | null = document.querySelector(
            'input[name="btnradio"]:checked'
        );

        if (checkedEl) {
            checkedEl.click();
        }
    }

    private async loadMovies(filmContainer: HTMLElement): Promise<void> {
        try {
            const arrayOfMovies: IMovie[] = await this.getMethodOfLoad();
            arrayOfMovies.forEach((movie: IMovie): void =>
                filmContainer.append(MovieCard(movie, this.likedMovies))
            );
            if (!this.randomMovie) this.setRandomMovie(arrayOfMovies);
        } catch (error) {
            console.log(error);
        }
    }

    private setRandomMovie(arrayOfMovies: IMovie[]): void {
        const randomMovie: IMovie =
            arrayOfMovies[Math.floor(Math.random() * arrayOfMovies.length)];
        this.randomMovie = randomMovie;
        const elem: HTMLElement = RandomMovie(randomMovie);
        document.querySelector('main')?.prepend(elem);
    }

    private getMethodOfLoad(): Promise<IMovie[]> {
        switch (this.mode) {
            case Mode.searchByName:
                return Movie.getMovieByName(this.text, this.page);
            case Mode.popularMovies:
                return Movie.getMoviesBySort(this.mode, this.page);
            case Mode.topRatedMovies:
                return Movie.getMoviesBySort(this.mode, this.page);
            case Mode.upcomingMovies:
                return Movie.getMoviesBySort(this.mode, this.page);
        }
    }

    private toggleFavoriteMovies(e: Event): void {
        const parentEl: HTMLElement = <HTMLElement>(
            (e.target as HTMLElement).parentElement
        );
        if (
            parentEl.getAttribute('class') !==
            'bi bi-heart-fill position-absolute p-2'
        ) {
            return;
        }

        const movieId: IMovie['id'] = Number(
            parentEl.getAttribute('data-film-id')
        );

        if (movieId) {
            const elements: NodeListOf<HTMLElement> = document.querySelectorAll(
                `svg[data-film-id='${movieId}']`
            );
            if (this.likedMovies.includes(movieId)) {
                this.likedMovies = this.likedMovies.filter(
                    (id: IMovie['id']): boolean => id !== movieId
                );
                elements.forEach((elem: HTMLElement): void => {
                    elem.setAttribute('fill', APP.HEARTCOL);
                });
            } else {
                this.likedMovies = [...this.likedMovies, movieId!];
                elements.forEach((elem: HTMLElement): void => {
                    elem.setAttribute('fill', APP.LIKEDHEARTCOL);
                });
            }
            Storage.setItem('likedMovies', JSON.stringify(this.likedMovies));
        }
    }

    private setMode(filmContainer: HTMLElement, mode: Mode): void {
        this.mode = mode;
        this.page = 1;
        filmContainer.innerHTML = '';
        const url: string[] = location.href.split('?');
        history.pushState(mode, '', url[0]);
    }

    private cleanContainer(container: HTMLElement): void {
        container.innerHTML = '';
    }
}

export default MovieApp;
