import * as ENV from '../../enums';
import { IMovie } from '../../interfaces/index';
import Http from '../http/http.service';
import { IOptions } from '../../interfaces';

class Movie {
    private _http: Http;
    private readonly BASE_MOVIE_URL: string = ENV.API.BASE_MOVIE_URL;

    constructor(http: Http) {
        this._http = http;
    }

    async getMoviesBySort(mode: ENV.Mode, page: number) {
        const url: string = this.BASE_MOVIE_URL + this.getUrlOfSort(mode);
        const method: string = 'GET';
        const query: IOptions['query'] = { page };
        const data = await this._http.load(url, { method, query });
        const results: IMovie[] = data.results.map(
            (movie: IMovie): IMovie => this.mapMovie(movie)
        );
        return results;
    }

    private getUrlOfSort(mode: ENV.Mode): string {
        switch (mode) {
            case ENV.Mode.popularMovies:
                return '/movie/popular';

            case ENV.Mode.upcomingMovies:
                return '/movie/upcoming';

            case ENV.Mode.topRatedMovies:
                return '/movie/top_rated';

            default:
                return '';
        }
    }

    async getMovieByName(text: string, page: number): Promise<IMovie[]> {
        const url: string = this.BASE_MOVIE_URL + '/search/movie';
        const method: string = 'GET';
        const query: IOptions['query'] = { query: text, page };
        const data = await this._http.load(url, { method, query });
        const results: IMovie[] = data.results.map(
            (movie: IMovie): IMovie => this.mapMovie(movie)
        );
        return results;
    }

    async getMovieById(id: IMovie['id']): Promise<IMovie> {
        const url: string = this.BASE_MOVIE_URL + `/movie/${id}`;
        const method: string = 'GET';
        const movie: IMovie = this.mapMovie(
            await this._http.load(url, { method })
        );
        return movie;
    }

    private mapMovie(movie: IMovie): IMovie {
        function getProperty<T, K extends keyof T>(obj: T, key: K): T[K] {
            return obj[key];
        }

        const mappedMovie: IMovie = {
            id: getProperty(movie, 'id'),
            overview: getProperty(movie, 'overview'),
            poster_path: getProperty(movie, 'poster_path'),
            release_date: getProperty(movie, 'release_date'),
            backdrop_path: getProperty(movie, 'backdrop_path'),
            original_title: getProperty(movie, 'original_title'),
        };

        return mappedMovie;
    }
}

export default Movie;
