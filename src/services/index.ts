import Http from './http/http.service';
import Movie from './movie/movie.service';
import LocalStorage from './storage/storage.service';

const http = new Http();

const movie = new Movie(http);

const storage = new LocalStorage(localStorage);

export { movie, storage };
