class LocalStorage {
    private readonly storage: Storage;
    constructor(storage: Storage) {
        this.storage = storage;
    }

    getItem(name: string) {
        return this.storage.getItem(name);
    }

    setItem(name: string, value: string): void {
        this.storage.setItem(name, value);
    }
}

export default LocalStorage;