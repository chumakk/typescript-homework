import * as ENV from '../../enums';
import { IError, IOptions } from '../../interfaces';

class Http {
    private readonly API_KEY: string = ENV.API.API_KEY;

    load(url: string, options: IOptions): Promise<any> {
        const { method, query } = options;
        return fetch(this.getUrl(url, query), {
            method,
        })
            .then(this.checkStatus)
            .then((response: Response) => response.json());
    }

    private getUrl(url: string, query?: IOptions['query']): string {
        const fullUrl: string = `${url}?${new URLSearchParams({
            ...query,
            api_key: this.API_KEY,
        }).toString()}`;

        return fullUrl;
    }

    private async checkStatus(response: Response): Promise<Response> {
        if (!response.ok) {
            const data = await response.json();
            const error: IError = {
                status: response.status,
                message: data.status_message,
            };
            throw error;
        }
        return response;
    }
}

export default Http;
