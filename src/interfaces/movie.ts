interface IMovie {
    id: number;
    overview: string | null;
    poster_path: string | null;
    release_date: string | undefined;
    backdrop_path: string | null;
    original_title: string;
}

export default IMovie;
