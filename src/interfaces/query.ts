interface IQuery {
    [name: string]: string | number;
}

interface IOptions {
    method: string;
    query?: IQuery;
}

export default IOptions;
