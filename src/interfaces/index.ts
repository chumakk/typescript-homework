import IMovie from './movie';
import IError from './error';
import IOptions from './query';

export { IMovie, IError, IOptions };
